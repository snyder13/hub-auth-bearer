#!/usr/bin/env node
'use strict';

/**
 * Usage: bearer.js /path/to/secrets.js https://targethubhost.org
 *
 * secrets.js is a file that looks like:
 * 	global.exports = {
 * 		'hub': {
 * 			'keys': {
 * 				'public': 'your API public key',
 * 				'private': 'your API private key'
 * 			}
 * 		}
 * 	};
 *
 * If all goes well this will output a base64 string representing your bearer authentication token.
 *
 *
 * To use this token with `curl` or another HTTP-based application, supply the
 * "Authorization: Bearer (output from this script)"
 * header on your request. e.g.,
 *
 * 	curl -H "Authorization: Bearer (output from this script)" https://targethubhost.org/services/someService?arg=42&anotherArg=2
 *
 * To use this token on the web, import the client library:
 *
 * 	<script data-bearer-token="(output from this script)" src="https://targethubhost.org/services/client-lib.js"></script>
 * 	<script>
 * 		document.addEventListener('hubapiready', (evt) => {
 * 			const hub = evt.detail; // hub has a handful of properties and methods related to the API service
 *				// example
 * 			hub.get('someService', { 'arg': 42, 'anotherArg': 2 })
 * 				.then(successCallback, failureCallback); // (define these callbacks yourself)
 * 		});
 * 	</script>
 *
 */
// nb: written with older style js in an effort to work with more node versions, and to not require npm globals
var
	crypto = require('crypto'),
	keys = global.hex ? hex.conf.get('hub.keys') : require(process.argv[2]).hub.keys,
	host = global.hex ? hex.conf.get('hub.host') : process.argv[3],
	svcPath = '/services/login/hmac-socket-token'
	;
if (host.indexOf('http') !== 0) {
	host = 'https://' + host;
}

module.exports = function(cb) {
	var dt = (new Date).toUTCString();

	var sig = crypto.createHmac('sha256', keys.private).update('POST');
	sig = crypto.createHmac('sha256', sig.digest()).update(svcPath);
	sig = crypto.createHmac('sha256', sig.digest()).update(dt);
	sig = crypto.createHmac('sha256', sig.digest()).update(crypto.createHash('md5').update('').digest());

	var req = require(host.indexOf('https') === 0 ? 'https' : 'http').request({
		'method': 'POST',
		'protocol': host.replace(/\/\/.*$/, ''),
		'hostname': host.replace(/^.*?:\/\//, ''),
		'path': svcPath,
		'headers': {
			'authorization': [ keys.public, dt, sig.digest('base64') ].join(';')
		}
	}, function(res) {
		var body = [];
		res.on('data', function(chunk) {
			body.push(chunk);
		});
		res.on('end', function() {
			body = Buffer.concat(body).toString('utf8');
			if (res.statusCode === 200) {
				cb(null, {
					'token': JSON.parse(body).token,
					'clientLib': host + '/services/client-lib.js'
				})
			}
			else {
				cb(body);
			}
		});
	});

	req.on('error', console.error);
	req.end();
}

if (!global.hex) {
	module.exports(function(err, res) {
		if (err) {
			throw err;
		}
		console.log(res.token);
	});
}
